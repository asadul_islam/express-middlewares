var express = require('express');
var bodyParser = require('body-parser');
var http = require('http');
var mongoose = require('mongoose');
mongoose.Promise = require("bluebird");
var debug = require('debug')('mean-app:server');
var compression = require('compression');
var cookieParser = require('cookie-parser');
var cors = require('cors');
var morgan = require('morgan');
var fs = require('fs');
var path = require('path');
var multer = require('multer');
var ejs = require('ejs');

var app = express();
app.set('view engine', 'ejs');

/***********User Modules *************/
/*************************************/
var config = require('./config/config.js');
var dbUrl = config.db.db_url;
var port = process.env.PORT || config.app.port;

var secretKeys = require('./config/secret.keys.js');

/*** Using Express Middleware *****/
/**********************************/
//body-parser
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

//compress response, reduce page load time
app.use(compression());

//cookie-parser
app.use(cookieParser(secretKeys.cookie));

//app.use(cors()) ; //enabling cors for all routes
//morgan
//app.use(morgan('combined'))
//app.use(morgan(':id :method :url :response-time')); //custom token
// create a write stream (in append mode);
var accessLogStream = fs.createWriteStream(path.join(__dirname, 'access.log'), { flags: 'a' })
app.use(morgan(setString, {
    skip: function(req, res) { return res.statusCode < 400 },
    stream: accessLogStream
}));

function setString(tokens, req, res) {
    return [
            tokens.method(req, res),
            tokens.url(req, res),
            tokens.status(req, res),
            tokens.res(req, res, 'content-length'), '-',
            tokens['response-time'](req, res), 'ms'
        ]
        .join(' ');
}

/******** Http Request Handling **********/
/*****************************************/
var index = require('./routes/index.js');
var bp = require('./routes/bodyParser.js');
//var cs = require('./routes/cookieSession');
var crs = require('./routes/cors');
var csrf = require('./routes/crsf');
var errHandler = require('./routes/errorHandler');
var uploads = require('./routes/multer');
var ses = require('./routes/session');
var vhost = require('./routes/vhost');
var helmet = require('helmet');

app.use('/', index);
app.use('/', bp);
///app.use('/', cs);
app.use('/', crs);
//app.use('/', csrf);
app.use('/', errHandler);
app.use('/', uploads);
app.use('/', ses);
app.use('/', vhost);

app.use(helmet.frameguard());
app.use(helmet.noCache());
app.use(helmet.contentSecurityPolicy({
    directives: {
        defaultSrc: ["'self'", 'localhost'],
        scriptSrc: ["'self'", "'unsafe-inline'"],
        sandbox: ['allow-forms', 'allow-scripts'],
        reportUri: '/report-violation',
        objectSrc: ["'none'"],
        upgradeInsecureRequests: true,
        workerSrc: false // This is not set.
    }
}));


// catch 404 error handler
app.use(function(req, res, next) {
    res.status(404).send({ "error_msg": "Page Not Found" });
});

// error handler
app.use(function(err, req, res, next) {
    // set locals, only providing error in development
    res.locals.message = err.message;
    res.locals.error = req.app.get('env') === 'development' ? err : {};

    // render the error page
    res.status(err.status || 500);
    res.send(err.message);
    console.log(err);
});

/***************************************/
/****** Server Connection Handling *****/
var server = http.createServer(app);
server.listen(port);
server.on('error', onError);
server.on('listening', onListening);
server.on('close', onClose);

function onError(error) {
    if (error.syscall !== 'listen') {
        throw error;
    }

    var bind = typeof port === 'string' ?
        'Pipe ' + port :
        'Port ' + port;

    // handle specific listen errors
    switch (error.code) {
        case 'EACCES':
            console.error(bind + ' requires elevated privileges');
            process.exit(1);
            break;
        case 'EADDRINUSE':
            console.error(bind + ' is already in use');
            process.exit(1);
            break;
        default:
            throw error;
    }
}

function onListening() {
    var addr = server.address();
    var bind = typeof addr === 'string' ?
        'pipe ' + addr :
        'port ' + addr.port;
    debug('Listening on ' + bind);
    console.log("Server running at port " + bind)
    mongoose.connect(dbUrl, connectionError);
}

function onClose(error) {
    if (error) {
        console.log(error);
    } else {
        console.log("Server Closed");
    }
    mongoose.disconnect(disconnectError);
}

/********** Database Connection Error Handling ***/
/*************************************************/
function connectionError(err) {
    if (err) {
        console.log("Can't connect database");
    } else {
        console.log("Database connected");
    }
}

function disconnectError(err) {
    if (err) {
        console.log("Can't disconnect database");
    } else {
        console.log("Database disconnected");
    }
}
/*******************************************************/