var app = require('express')();
var bodyParser = require('body-parser');

var urlencodedParser = bodyParser.urlencoded({ extended: false });
var jsonParser = bodyParser.json();

var secretKeys = require('../config/secret.keys.js');

// parse various different custom JSON types as JSON
app.use(bodyParser.json({ type: 'application/*+json' }));
// parse some custom thing into a Buffer
app.use(bodyParser.raw({ type: 'application/vnd.custom-type' }));
// parse an HTML body into a string
app.use(bodyParser.text({ type: 'text/html' }));
app.use(bodyParser.text({ type: 'text/plain' }));
app.use(bodyParser.text({ type: 'text/xml' }));
app.use(bodyParser.text({ type: 'application/xml' }));
app.use(bodyParser.text({ type: 'application/javascript' }));

// POST  gets urlencoded bodies
app.post('/bp/post', urlencodedParser, function(req, res) {
	if (!req.body) return res.sendStatus(400);
	if (!req.cookies['name']) {
		res.cookie("name", req.body.name, { maxAge: 900000, httpOnly: true }).send("Cookie Set");
	} else
		res.send(req.cookies['name']);
});

// POST /api/users gets JSON bodies
app.get('/bp/json', jsonParser, function(req, res) {
	if (!req.body) return res.sendStatus(400);
	if (!req.signedCookies['name']) {
		res.cookie("name", "Asad", { maxAge: 9000555500, httpOnly: true, signed: true });
		//res.cookie("email", "asad@gmail.com", { maxAge: 900000, httpOnly: true });
		res.send("Cookie Set");
	} else
		res.send(req.signedCookies);
	// create user in req.body
});

app.get('/bp/logout', function(req, res) {
	res.clearCookie('name');
	res.send('cookie cleared');
});

app.post('/bp/text', function(req, res) {
	if (!req.body) return res.sendStatus(400);
	res.send(req.body);
});

function setCookie(name, value, options) {
	return { name, value, options };
}

module.exports = app;