var csrf = require('csurf');
var app = require('express')();
//var csrfProtection = csrf({ cookie: true });

app.post('/csrf/fg', function(req, res, next) {
    res.send("No need csrf token");
});

app.use(csrf({ cookie: true }));

app.get('/csrf/form', function(req, res) {
    // pass the csrfToken to the view
    res.send({ csrfToken: req.csrfToken() });
})

app.post('/csrf/process', function(req, res) {
    res.send('data is being processed');
});

module.exports = app;