var app = require('express')();
var vhost = require('vhost');

app.use(vhost('*.*.localhost', function handle(req, res, next) {
    // for match of "foo.bar.localhost:8080" against "*.*.example.com":
    console.dir(req.vhost.host) // => 'foo.bar.localhost:8080'
    console.dir(req.vhost.hostname) // => 'foo.bar.localhost'
    console.dir(req.vhost.length) // => 2
    console.dir(req.vhost[0]) // => 'foo'
    console.dir(req.vhost[1]) // => 'bar'
}));


module.exports = app;