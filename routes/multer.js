let multer = require('multer');
let app = require('express')();

let storage = multer.diskStorage({
    destination: function(req, file, cb) {
        cb(null, './uploads/')
    },
    filename: function(req, file, cb) {
        cb(null, file.fieldname + '-' + Date.now())
    }
});
let upload = multer({ storage: storage });

app.get('/profileUpload', function(req, res, next) {
    res.render('../views/uploads');
});

app.post('/profileUpload', upload.single('avatar'), function(req, res, next) {

    res.send('file uploaded');

});

app.post('/multiUpload', upload.array('photos', 3), function(req, res, next) {
    res.send("Uploaded");
});

module.exports = app;