var app = require('express')();
var errorHandler = require('errorhandler');
var notifier = require('node-notifier');

app.use(errorHandler({ log: errorHandle }));

function errorHandle(err, str, req, res) {
    var error = "Error in " + req.method + " " + req.url;

    notifier.notify({
        title: error,
        message: str
    });
}

app.post('/err/login', function(req, res, next) {
    res.send("Your Info");
});

module.exports = app;