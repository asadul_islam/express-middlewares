var app = require('express')();
var cookieSession = require('cookie-session');
var Keygrip = require('keygrip');
var secretKeys = require('../config/secret.keys');

app.set('trust proxy', 1);

app.use(cookieSession({
    secret: secretKeys.cookie,
    keys: new Keygrip(['key1', 'key2'], 'SHA384', 'base64'), //custom algorithm
    name: "loginInfo",
    maxAge: 300000000,
    expires: 300000000,
    path: '/cs/login',
    sameSite: false,
    secure: false,
    httpOnly: true,
    signed: true,
    overwrite: true
}));

app.get("/cs/login", function(req, res, next) {
    if (req.session.isNew) {
        req.session.email = "asad@gmail.com";
        req.session.password = "password";
        req.session.phone = "01XXXXXXXX";
        req.session.user = "Asad";

        req.sessionOptions.maxAge = req.session.maxAge || req.sessionOptions.maxAge;
        res.send("Cookie Set");
    } else {
        req.session.nowInMinutes = Math.floor(Date.now() / 60e3); //extending session
        res.send("Your email " + req.session.email + " " + req.session.password + " " + req.session.phone + " " + req.session.user);
    }
});

app.get('/cs/logout', function(req, res, next) {
    req.session = null;
    res.send("Cookies deleted");
});

module.exports = app;