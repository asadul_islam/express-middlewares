var app = require('express')();
var cors = require('cors');

//enabling cors for single route
app.get('/cors', cors(), function(req, res, next) {
    res.send("Cors enabled for this route");
});

app.get('/cors/sp', cors(corsOptions), function(req, res, next) {
    res.send("This cors enabled for only the specific domain");
});

app.get('/cors/do', function(req, res, next) {
    res.send("Dynamic origin.");
});

/****Configuring Cors *******/
//specific origin
var corsOptions = {
    origin: 'http://example.com',
    optionsSuccessStatus: 200 // some legacy browsers (IE11, various SmartTVs) choke on 204
}

//dynamic origin
var whitelist = ['http://example1.com', 'http://example2.com']
var corsOptions1 = {
    origin: function(origin, callback) {
        if (whitelist.indexOf(origin) !== -1) {
            callback(null, true)
        } else {
            callback(new Error('Not allowed by CORS'))
        }
    }
}


module.exports = app;