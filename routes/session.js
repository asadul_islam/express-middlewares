var app = require('express')();
var secretKeys = require('../config/secret.keys');

var session = require('express-session');
app.set('trust proxy', 1);
app.use(session({
    secret: secretKeys.cookie,
    resave: true,
    saveUninitialized: true,
    cookie: { maxAge: 60 * 60 * 24 * 30 }
}));

// Access the session as req.session
app.get('/session', function(req, res, next) {
    if (req.session.views) {
        req.session.views++;
        res.setHeader('Content-Type', 'text/html');
        res.write('<p>views: ' + req.session.views + '</p>');
        res.write('<p>expires in: ' + (req.session.cookie.maxAge / 1000) + 's</p>');
        res.end();
    } else {
        req.session.views = 1;
        res.end('welcome to the session demo. refresh!');
    }
})

module.exports = app;